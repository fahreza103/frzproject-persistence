package com.frzproject.springpersistence.model.criteria;

import javax.persistence.criteria.JoinType;

import com.frzproject.springpersistence.constant.CriteriaOperator;


public class CriteriaAtomicManagedResource extends CriteriaManagedResource {
	private String field;
	private CriteriaOperator operator;
	private String value;
	private JoinType joinType;
	

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public CriteriaOperator getOperator() {
		return operator;
	}

	public void setOperator(CriteriaOperator operator) {
		this.operator = operator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public JoinType getJoinType() {
		return joinType;
	}

	public void setJoinType(JoinType joinType) {
		this.joinType = joinType;
	}

	@Override
	public boolean isEmpty() {
		return value == null && operator == null && field == null;
	}
}
