package com.frzproject.springpersistence.model.criteria;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.frzproject.springpersistence.model.pagination.PagingRequest;
import com.frzproject.springpersistence.model.pagination.SortCriteria;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "group", defaultImpl = CriteriaAtomicManagedResource.class)
@JsonSubTypes({ @Type(value = AndCriteriaManagedResource.class, name = "AND"),
		@Type(value = OrCriteriaManagedResource.class, name = "OR") })
public abstract class CriteriaManagedResource {

	private List<SortCriteria> orderBy;
	
	private PagingRequest pagingRequest;
	
	private Map<String,Object> additionalFilter;
	
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public List<SortCriteria> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(List<SortCriteria> orderBy) {
		this.orderBy = orderBy;
	}



	public PagingRequest getPagingRequest() {
		return pagingRequest;
	}

	public void setPagingRequest(PagingRequest pagingRequest) {
		this.pagingRequest = pagingRequest;
	}

	public Map<String, Object> getAdditionalFilter() {
		return additionalFilter;
	}

	public void setAdditionalFilter(Map<String, Object> additionalFilter) {
		this.additionalFilter = additionalFilter;
	}


	
	
	
}
