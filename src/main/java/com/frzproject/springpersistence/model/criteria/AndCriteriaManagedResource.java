package com.frzproject.springpersistence.model.criteria;


public class AndCriteriaManagedResource extends CriteriaGroupManagedResource {
	
	public AndCriteriaManagedResource() {
		super("AND");
	}

}
