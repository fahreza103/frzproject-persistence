package com.frzproject.springpersistence.model.criteria;


public class OrCriteriaManagedResource extends CriteriaGroupManagedResource {
	
	public OrCriteriaManagedResource() {
		super("OR");
	}
}
