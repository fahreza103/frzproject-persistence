package com.frzproject.springpersistence.model.pagination;

import com.frzproject.springpersistence.constant.SortType;

public class SortCriteria {

	public String fieldName;
	public SortType sortType;
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public SortType getSortType() {
		return sortType;
	}
	public void setSortType(SortType sortType) {
		this.sortType = sortType;
	}
	
	
}
