package com.frzproject.springpersistence.model.pagination;

public class PagingRequest {

	private int size;
	
	private int currentPage;

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	
	
}
