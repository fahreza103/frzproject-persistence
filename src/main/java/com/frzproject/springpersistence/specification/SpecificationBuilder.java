package com.frzproject.springpersistence.specification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EmbeddedId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.frzproject.springpersistence.constant.CriteriaOperator;
import com.frzproject.springpersistence.constant.SortType;
import com.frzproject.springpersistence.model.criteria.AndCriteriaManagedResource;
import com.frzproject.springpersistence.model.criteria.CriteriaAtomicManagedResource;
import com.frzproject.springpersistence.model.criteria.CriteriaManagedResource;
import com.frzproject.springpersistence.model.criteria.OrCriteriaManagedResource;
import com.frzproject.springpersistence.model.pagination.SortCriteria;
import com.frzproject.springpersistence.util.ReflectionUtil;
import com.frzproject.springpersistence.util.StringUtil;




/**
 * Dynamic specifications for dynamic searching API, all field that has value will be used to filter search
 * @author Fahreza Tamara
 * @param <T>
 */
public class SpecificationBuilder<T> {

	protected CriteriaManagedResource criteriaManagedResource;
	
	protected long totalItems = 0;
	
	protected Map<String,From<?,?>> joinMap = new HashMap<String, From<?, ?>>();
	
	public SpecificationBuilder() {}
	
	public SpecificationBuilder(CriteriaManagedResource criteriaManagedResource) {
		super();
		this.criteriaManagedResource = criteriaManagedResource;
	}
	

	public CriteriaManagedResource getCriteriaManagedResource() {
		return criteriaManagedResource;
	}

	public void setCriteriaManagedResource(CriteriaManagedResource criteriaManagedResource) {
		this.criteriaManagedResource = criteriaManagedResource;
	}
	
	/**
	 * build a specification search, no order
	 * @return Specification
	 */
	public  Specification<T> buildSpecication () {
		return buildSpecication(null);
	}

	/**
	 * build a specification search, manually order
	 * @return Specification
	 */
	public  Specification<T> buildSpecication (List<SortCriteria> orderBy ) {
		return new Specification<T>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 9148907144015215123L;

			@Override
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				joinMap = new HashMap<String, From<?, ?>>(); 
				List<Predicate> predicates = new ArrayList<>();
				Predicate predicate = buildCriteria(criteriaManagedResource, root, query, criteriaBuilder);
				if(predicate !=null) predicates.add(predicate);
				List<Predicate> additionalPredicates = addCustomSpecification(root,query,criteriaBuilder);
				if(additionalPredicates.size() > 0) predicates.addAll(additionalPredicates);
				Predicate finalResult = criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				query.distinct(true);
				
				if(orderBy != null)
					doOrder(root,query,criteriaBuilder,orderBy);
				
				//query.orderBy(criteriaBuilder.asc(criteriaBuilder.literal(5)));
				return finalResult;
			}
		};
	}
	
	/**
	 * Get specified path , if the field contains multiple values separated by dot, it will perform join operations or if it
	 * contains EmbeddedId annotations, will use the field inside the composite primary key class
	 * @param root
	 * @param fieldName
	 * @return
	 */
	protected Map<String,Object> getSpecificPathByFieldName(Root<T> root, String fieldName, JoinType joinType) {
		Map<String,Object> result = new HashMap<String, Object>();
		if(fieldName.contains(".")) {
			String[] fields = fieldName.split("\\.");
			From<?, ?> joinFrom = null;
			Path<?> path = null;
			// it is composite primary key in different class, should not join 
			if(ReflectionUtil.checkFieldHasAnnotation(root.getJavaType(), fields[0], EmbeddedId.class)) {
				path = root.get(fields[0]);
				for(int i=1; i<fields.length;i++) {
					path = path.get(fields[i]);
				}
				result.put("from",root);
			} else {
				joinFrom = joinCriteria(fields[0], root,joinType);
				for(int i=1; i<fields.length-1;i++) {
					joinFrom = joinCriteria(fields[i], joinFrom,joinType);
				}
				path = joinFrom.get(fields[fields.length-1]);
				result.put("from",joinFrom);
			}
			result.put("path",path);
			result.put("fieldName", fields[fields.length-1]);
		} else {
			result.put("from",root);
			result.put("path",root.get(fieldName));
			result.put("fieldName", fieldName);
			
		}
		return result;
	}
	
	/**
	 * add OrderBy Clause based on parameter , same as filter, if the field contain dot for separator, it will join to another table
	 * Do filter per field and what sort to be executed
	 * @param root
	 * @param query
	 * @param criteriaBuilder
	 * @param isDescending
	 * @param orderBy
	 */
	protected void doOrder(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, List<SortCriteria> orderBy ) {
		List<Order> orderList = new ArrayList<Order>();
		for(SortCriteria order : orderBy) {
			// Order from another table, join table, or composite primary key
			if(order.getFieldName().contains(".")) {
				query.distinct(false);
			}
			
			Map<String,Object> pathMap = getSpecificPathByFieldName(root, order.getFieldName(),null);
			Path<?> specificPath = (Path<?>) pathMap.get("path");
			if(order.getSortType()==SortType.ASC) {
				orderList.add(criteriaBuilder.asc(specificPath));
			} else {
				orderList.add(criteriaBuilder.desc(specificPath));
			}
		}
		query.orderBy(orderList);
	}

	/**
	 * Create join criteria based on annotation OneToOne or OneToMany inside field of the entity
	 * @param fieldName
	 * @param root
	 * @return
	 */
	protected From<?,?> joinCriteria(String fieldName, From<?,?> root, JoinType joinType) {
		From<?,?> join = joinMap.get(fieldName);
		if(join == null) {
			if(ReflectionUtil.checkFieldHasAnnotation(root.getJavaType(), fieldName, OneToOne.class)) {
				join = root.join(fieldName, joinType == null ? JoinType.INNER : joinType);
			} else if (ReflectionUtil.checkFieldHasAnnotation(root.getJavaType(), fieldName, OneToMany.class)) {
				join = root.joinSet(fieldName, joinType == null ? JoinType.LEFT : joinType);
			} else {
				join = root.joinSet(fieldName, joinType == null ? JoinType.LEFT : joinType);
			}
			if(join != null) {
				joinMap.put(fieldName,join);
			}
		}	
		return join;
	}
	


	/**
	 * Build search criteria by CriteriaManagedResource, it will gather all groups into the deepest child using recursion
	 * @param criteriaManagedRoot
	 * @param root
	 * @param query
	 * @param criteriaBuilder
	 * @return
	 */
	protected Predicate  buildCriteria( CriteriaManagedResource criteriaManagedRoot, Root<T> root,
			CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		if(criteriaManagedRoot instanceof CriteriaAtomicManagedResource) {
			CriteriaAtomicManagedResource criteriaAtomic = (CriteriaAtomicManagedResource) criteriaManagedRoot;
			if(criteriaAtomic.getField() != null) {
				Map<String,Object> pathMap = getSpecificPathByFieldName(root, criteriaAtomic.getField(),criteriaAtomic.getJoinType());
				Path<?> path = (Path<?>) pathMap.get("path");
				From<?,?> from = (From<?,?>) pathMap.get("from");
				return createPredicateByOperator(criteriaAtomic.getOperator(), (String)pathMap.get("fieldName"), 
						criteriaAtomic.getValue(), from,path, criteriaBuilder);
			} else {
				return null;
			}
		} else if(criteriaManagedRoot instanceof AndCriteriaManagedResource) {
			AndCriteriaManagedResource andCriteriaManagedResource = (AndCriteriaManagedResource) criteriaManagedRoot;
			List<Predicate> predicatesPerGroup = new ArrayList<Predicate>();
			for(CriteriaManagedResource criteriaManagedResource : andCriteriaManagedResource.getCriterias()) {
				Predicate criteria =buildCriteria(criteriaManagedResource,root,query,criteriaBuilder);
				if(criteria != null) predicatesPerGroup.add(criteria);
			}
			return predicatesPerGroup.size() > 0 ?criteriaBuilder.and(predicatesPerGroup.toArray(new Predicate[predicatesPerGroup.size()])) : null;
		} else {
			OrCriteriaManagedResource orCriteriaManagedResource = (OrCriteriaManagedResource) criteriaManagedRoot;
			List<Predicate> predicatesPerGroup = new ArrayList<Predicate>();
			for(CriteriaManagedResource criteriaManagedResource : orCriteriaManagedResource.getCriterias()) {
				Predicate criteria =buildCriteria(criteriaManagedResource,root,query,criteriaBuilder);
				if(criteria != null) predicatesPerGroup.add(criteria);
			}
			return predicatesPerGroup.size() > 0 ?criteriaBuilder.or(predicatesPerGroup.toArray(new Predicate[predicatesPerGroup.size()])):null;
		}
	}
	
	/**
	 * Create single criteria by param string of value, and operator enum
	 * @param criteriaAtomic
	 * @param root
	 * @param criteriaBuilder
	 * @return
	 */
	protected  Predicate createPredicateByOperator(CriteriaOperator operator , String field, String value, From<?,?> root,Path<?>path,CriteriaBuilder criteriaBuilder) {
		CriteriaAtomicManagedResource criteria = new CriteriaAtomicManagedResource();
		criteria.setField(field);
		criteria.setOperator(operator);
		criteria.setValue(value);
		return createPredicateByOperator(criteria, root,path, criteriaBuilder);
	}

	/**
	 * Create single criteria by CriteriaAtomicManagedResource (contains value and operator)
	 * @param criteriaAtomic
	 * @param root
	 * @param criteriaBuilder
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected  Predicate createPredicateByOperator(CriteriaAtomicManagedResource criteriaAtomic, From<?,?> root,Expression<?> path,CriteriaBuilder criteriaBuilder) {
		if(criteriaAtomic.getValue() == null || "".equals(criteriaAtomic.getValue())) {
			return null;
		} 
		Class<?> dataType = ReflectionUtil.getFieldType(root.getJavaType(), criteriaAtomic.getField());
		Object value =  StringUtil.figureDataType(criteriaAtomic.getValue(),dataType);

		
		// For IN Operations
		List<String> multipleValues = new ArrayList<String>(Arrays.asList(criteriaAtomic.getValue().toUpperCase().split(";")));
		Predicate predicate = null;
		if(criteriaAtomic.getOperator() == CriteriaOperator.equal) {
			predicate = value instanceof String ? criteriaBuilder.equal(criteriaBuilder.upper((Expression<String>) path), value) :
				criteriaBuilder.equal(path, value);
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.notEqual) {
			predicate = value instanceof String ? criteriaBuilder.notEqual(criteriaBuilder.upper((Expression<String>) path), value) :
				criteriaBuilder.notEqual(path, value);
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.greaterThan) {
			predicate = value instanceof Date ? criteriaBuilder.greaterThan((Expression<Date>)path, (Date)value) :
				criteriaBuilder.greaterThan((Expression<Double>)path, (Double)value);
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.greaterThanOrEqualTo) {
			predicate = value instanceof Date ? criteriaBuilder.greaterThanOrEqualTo((Expression<Date>)path, (Date)value) :
				criteriaBuilder.greaterThanOrEqualTo((Expression<Double>)path, (Double)value);
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.lessThan) {
			predicate = value instanceof Date ? criteriaBuilder.lessThan((Expression<Date>)path, (Date)value) :
				criteriaBuilder.lessThan((Expression<Double>)path, (Double)value);
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.lessThanOrEqualTo) {
			predicate = value instanceof Date ? criteriaBuilder.lessThanOrEqualTo((Expression<Date>)path, (Date)value) :
				criteriaBuilder.lessThanOrEqualTo((Expression<Double>) path, (Double)value);
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.in) {
			predicate = criteriaBuilder.in(root.get(criteriaAtomic.getField())).value(multipleValues);
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.notIn) {
			predicate = criteriaBuilder.in(root.get(criteriaAtomic.getField())).value(multipleValues);
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.like) {
			predicate = criteriaBuilder.like(criteriaBuilder.upper((Expression<String>) path), "%"+criteriaAtomic.getValue().toString().toUpperCase()+"%");
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.notLike) {
			predicate = criteriaBuilder.notLike(criteriaBuilder.upper(root.get(criteriaAtomic.getField())), "%"+criteriaAtomic.getValue().toString().toUpperCase()+"%");
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.isEmpty) {
			predicate = criteriaBuilder.isEmpty(root.get(criteriaAtomic.getField()));
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.isNotEmpty) {
			predicate = criteriaBuilder.isNotEmpty(root.get(criteriaAtomic.getField()));
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.isNull) {
			predicate = criteriaBuilder.isNull(root.get(criteriaAtomic.getField()));
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.isNotNull) {
			predicate = criteriaBuilder.isNotNull(root.get(criteriaAtomic.getField()));
		} else if(criteriaAtomic.getOperator() == CriteriaOperator.between) {
			Object value1 =  StringUtil.figureDataType(multipleValues.get(0),null);
			Object value2 =  StringUtil.figureDataType(multipleValues.get(1),null);

			predicate = value1 instanceof Date ? criteriaBuilder.between(root.get(criteriaAtomic.getField()), (Date)value1, (Date)value2) :
				criteriaBuilder.between(root.get(criteriaAtomic.getField()), (Double)value1, (Double)value2);
		}
		return predicate;
	}
	
 

	protected long getTotalItems() {
		return totalItems;
	}

	/**
	 * Added specifications that should be implemented in subclass for each entity
	 * @param object
	 * @param predicates
	 */
	protected List<Predicate> addCustomSpecification(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		return new ArrayList<Predicate>();
	}
	
}
