package com.frzproject.springpersistence.constant;

public enum CriteriaOperator {
	notIn, in, notEqual, equal, greaterThan, greaterThanOrEqualTo, lessThan, lessThanOrEqualTo, notLike, like, isNull, isNotNull, isEmpty, isNotEmpty,between;

	public static CriteriaOperator fromString(String text) {
		switch (text) {
		case "notIn":
		case "not_in":
			return CriteriaOperator.notIn;
		case "in":
			return CriteriaOperator.in;
		case "notEq":
		case "not_equal":
			return CriteriaOperator.notEqual;
		case "eq":
		case "equal":
			return CriteriaOperator.equal;
		case ">":
		case "greater_than":
			return CriteriaOperator.greaterThan;
		case ">=":
		case "greater_than_or_equal":
			return CriteriaOperator.greaterThanOrEqualTo;
		case "<":
		case "lesser_than":
			return CriteriaOperator.lessThan;
		case "<=":
		case "lesser_than_or_equal":
			return CriteriaOperator.lessThanOrEqualTo;
		case "like":
			return CriteriaOperator.like;
		case "notLike":
		case "not_like":
			return CriteriaOperator.notLike;
		case "is_null":
		case "isNull":
			return CriteriaOperator.isNull;
		case "isNotNull":
			return CriteriaOperator.isNotNull;
		case "isEmpty":
			return CriteriaOperator.isEmpty;
		case "isNotEmpty":
			return CriteriaOperator.isNotEmpty;
		case "between":
			return CriteriaOperator.between;
		default:
			throw new RuntimeException(String.format("Invalid search operator: %s", text));

		}
	}

}
