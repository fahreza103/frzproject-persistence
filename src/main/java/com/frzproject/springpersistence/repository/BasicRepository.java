package com.frzproject.springpersistence.repository;




import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;


@NoRepositoryBean
public interface BasicRepository<T> extends  JpaRepository<T,Long>,PagingAndSortingRepository<T,Long>,JpaSpecificationExecutor<T>{	

}
