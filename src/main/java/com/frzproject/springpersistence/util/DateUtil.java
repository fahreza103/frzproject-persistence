package com.frzproject.springpersistence.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

/**
 * Helper class to manage base entity type Date.
 * @author Fahreza Tamara
 */
public class DateUtil {
	
	public static final Date END_OF_TIME = asDate(LocalDate.of(2101, 1, 1));

	/** To be used with {@link #formatStr(Date, String)}. According to SimpleDateFormat. */
	public static final String FORMAT_TIMESTAMP = "yyyy-MM-dd_HH:mm:ss(z)";

	public static Date asDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public static Date asDate(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Get a <code>Date</code> representing the date/time at the start of today.
	 * 
	 * @return today's <code>Date</code>.
	 */
	public static Date today() {
		LocalDate today = LocalDate.now();
		return asDate(today);
	}

	/**
	 * Get a <code>Date</code> representing the date/time at the current moment.
	 * 
	 * @return now's <code>Date</code>.
	 */
	public static Date now() {
		LocalDateTime now = LocalDateTime.now();
		return asDate(now);
	}

	/**
	 * Get a <code>Date</code> representing the date/time at the start of tomorrow.
	 * 
	 * @return tomorrow's <code>Date</code>.
	 */
	public static Date tomorrow() {
		LocalDate today = LocalDate.now();
		LocalDate tomorrow = today.plus(1, ChronoUnit.DAYS);
		return asDate(tomorrow);
	}

	public static LocalDate asLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public static LocalDateTime asLocalDateTime(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	/**
	 * Check whether a date is between startDate and endDate.
	 * This method will ignore time by truncating the time part of every Date object
	 * 
	 * @param startDate
	 *            start date of the range
	 * @param endDate
	 *            end date of the range. endDate can be null to represent infinity
	 * @param date
	 *            the date to be checked
	 * @param strict
	 *            parameter to specify strictness of range check
	 * @return true if this date is between startDate and endDate; if strict is
	 *         true, only returns true when this date is strictly between the two
	 *         dates; if startDate or endDate is null, this date is considered
	 *         within the valid range.
	 */
	public static boolean withinValidPeriod(Date startDate, Date endDate, Date date, Boolean strict) {
		Date truncatedStartDate = DateUtils.truncate(startDate, Calendar.DATE);
		Date truncatedEndDate;
		if(endDate == null) {
			truncatedEndDate = null;
		} else {
			truncatedEndDate = DateUtils.truncate(endDate, Calendar.DATE);
		}
		Date truncatedDate = DateUtils.truncate(date, Calendar.DATE);
		System.out.println("truncated start date : " + truncatedStartDate);
		System.out.println("truncated end date : " + truncatedEndDate);
		System.out.println("truncated date : " + truncatedDate);
		if (strict == true) {
			return (truncatedStartDate == null || truncatedDate.after(truncatedStartDate)) && (truncatedEndDate == null || truncatedDate.before(truncatedEndDate));
		} else {
			return (truncatedStartDate == null || truncatedDate.compareTo(truncatedStartDate) >= 0)
					&& (truncatedEndDate == null || truncatedDate.compareTo(truncatedEndDate) <= 0);
		}
	}

	/**
	 * Check whether a date is between startDate and endDate, both ends inclusive. This method will ignore time
	 * 
	 * @param startDate
	 *            start date of the range
	 * @param endDate
	 *            end date of the range. endDate can be null to represent infinity
	 * @param date
	 *            the date to be checked
	 * @return true if this date is between startDate and endDate; if startDate or
	 *         endDate is null, this date is considered within the valid range.
	 */
	public static boolean withinValidPeriod(Date startDate, Date endDate, Date date) {
		return withinValidPeriod(startDate, endDate, date, false);
	}

	public static Date dateOf(int year, int month, int day) {
		return DateUtil.asDate(LocalDate.of(year, month, day));
	}

	public static Date plusDays(Date startDate, int dayToAdd) {
		LocalDate date = asLocalDate(startDate);

		return asDate(date.plusDays(dayToAdd));
	}

	public static Date plusMinutes(Date startDate, long minutesToAdd) {
		LocalDateTime date = asLocalDateTime(startDate);

		return asDate(date.plusMinutes(minutesToAdd));
	}

	public static int get(Date date, int field) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);

		return calender.get(field) + (field == Calendar.MONTH ? 1 : 0);
	}

	public static Date plusMonths(Date startDate, int monthToAdd) {
		LocalDate date = asLocalDate(startDate);

		return asDate(date.plusMonths(monthToAdd));
	}

	public static long getTotalMonthsDifference(Date startDate, Date endDate) {
		LocalDate firstDate = asLocalDate(startDate);
		LocalDate secondDate = asLocalDate(endDate);
		Period period = firstDate.until(secondDate);
		return period.toTotalMonths();
	}

	public static long getTotalDaysDifference(Date startDate, Date endDate) {
		LocalDate firstDate = asLocalDate(startDate);
		LocalDate secondDate = asLocalDate(endDate);
		return Duration.between(firstDate.atTime(0, 0), secondDate.atTime(0, 0)).toDays();
	}

	public static int getDaysDifferent(Date startDate, Date endDate) {
		LocalDate firstDate = asLocalDate(startDate);
		LocalDate secondDate = asLocalDate(endDate);
		Period period = firstDate.until(secondDate);
		return period.getDays();
	}

	public static int getNumberOfDays(Date date) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);

		// Get the number of days in that month
		return calender.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Check whether two time periods have overlap accurate to milliseconds Null end
	 * dates denotes infinity.
	 * 
	 * @param fromDate1
	 * @param toDate1
	 * @param fromDate2
	 * @param toDate2
	 * @return true if the two time periods overlap; false if fromDate is after
	 *         toDate; false otherwise
	 */
	public static boolean isOverlap(Date fromDate1, Date toDate1, Date fromDate2, Date toDate2) {
		Date toDate1copy = toDate1;
		Date toDate2copy = toDate2;
		if (toDate1 == null)
			toDate1copy = new Date(Long.MAX_VALUE);
		if (toDate2 == null)
			toDate2copy = new Date(Long.MAX_VALUE);
		return fromDate1.compareTo(toDate2copy) <= 0 && toDate1copy.compareTo(fromDate2) >= 0;
	}

	/** Check if date1 is before date2, allowing null to represent infinity. */
	public static boolean isBefore(Date date1, Date date2, boolean infiniteNull) {
		Date date1copy = date1;
		Date date2copy = date2;
		if (infiniteNull) {
			date1copy = date1 == null ? new Date(Long.MAX_VALUE) : new Date(date1.getTime());
			date2copy = date2 == null ? new Date(Long.MAX_VALUE) : new Date(date2.getTime());
		}
		// compare the copy instead of changing the original date
		// as java.util.Date is not immutable!
		return date1copy.before(date2copy);

	}

	/**
	 * Format a date object in a specified format
	 * 
	 * @param date
	 *            date to be formatted
	 * @param format
	 *            the format
	 * @return the date formatted if successful else returns null
	 */
	public static Date format(Date date, String format) {
		SimpleDateFormat simpleFormatter = new SimpleDateFormat(format);
		Date billDateFormatted;
		try {
			billDateFormatted = simpleFormatter.parse(simpleFormatter.format(date));
		} catch (ParseException e) {
			return null;
		}

		return billDateFormatted;
	}

	/**
	 * Format a date object in a specified format
     * see {@link #FORMAT_TIMESTAMP}
	 *
	 * @param date
	 *            date to be formatted
	 * @param format
	 *            the format
	 * @return the formatted date
	 */
	public static String formatStr(Date date, String format) {
		SimpleDateFormat simpleFormatter = new SimpleDateFormat(format);
		return simpleFormatter.format(date);
	}


	public static double calculateMonthlyProratedRatio(Date startChargeDate, Date endChargeDate) {
		int noofDaysForTheMonth = DateUtil.getNumberOfDays(startChargeDate);
		long noofDaysDifferents = DateUtil.getDaysDifferent(startChargeDate, endChargeDate) + 1;

		return calculateRatio(noofDaysForTheMonth, noofDaysDifferents);
	}

	public static double calculateRatio(double totalDays, double ratioDays) {
		if (totalDays == ratioDays) {
			return 0;
		}
		return ratioDays / totalDays;
	}

	/**
	 * Returns a Date set to the last possible millisecond of the day, just before
	 * midnight. If a null day is passed in, a new Date is created. midnight (00m
	 * 00h 00s)
	 * 
	 * @param day
	 * @param cal
	 * @return a Date set to the last possible millisecond of the day
	 */
	public static Date getEndOfDay(Date day) {
		Calendar cal = Calendar.getInstance();
		if (day == null)
			day = today();
		cal.setTime(day);
		cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
		return cal.getTime();
	}

	/**
	 * Returns a Date set to the first possible millisecond of the day, just after
	 * midnight. If a null day is passed in, a new Date is created. midnight (00m
	 * 00h 00s)
	 * 
	 * @param day
	 * @param cal
	 * @return
	 */
	public static Date getStartOfDay(Date day) {
		Calendar cal = Calendar.getInstance();
		if (day == null)
			day = today();
		cal.setTime(day);
		cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
		return cal.getTime();
	}
}
