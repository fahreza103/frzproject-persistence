package com.frzproject.springpersistence.util;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import org.springframework.util.ReflectionUtils;

/**
 * Utility class to perform reflection to object class, used to extract field, check annotations, etc
 * @author Fahreza Tamara
 *
 */
public class ReflectionUtil {

	/**
	 * Check if the field inside class has specified annotation class
	 * @param <T>
	 * @param clazz
	 * @param fieldName
	 * @param annotationClazz
	 * @return
	 */
	public static <T extends Annotation> boolean checkFieldHasAnnotation(Class<?> clazz , String fieldName, Class<T> annotationClazz) {
		try {
			Field field = clazz.getDeclaredField(fieldName);
			T annotation = field.getAnnotation(annotationClazz);
			if(annotation == null) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static Class<?> getFieldType(Class<?> clazz , String fieldName) {
		try {
			Field field = ReflectionUtils.findField(clazz, fieldName);
			return field.getType();
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return null;
	}


}
