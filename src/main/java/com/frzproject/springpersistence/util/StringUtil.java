package com.frzproject.springpersistence.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;

public class StringUtil {
	
	public static Object figureDataType(String strNum, Class<?> dataType) {
		Object result = strNum;
	    if (strNum == null) {
	        return null;
	    }
	    // dataType String, force to return String
	    if(dataType != null && dataType.isInstance(String.class)) {
	    	return strNum;
	    }
	    
	    // Figure if this string could be change to numeric
	    try {
	    	result = Double.parseDouble(strNum);
	    } catch (NumberFormatException nfe) {}
	    
	    // Figure if this string could be change to date yyyy-MM-dd
	    try {
			result = new SimpleDateFormat("yyyy-MM-dd").parse(strNum);
		} catch (ParseException e) {}
	    
	    return result;
	}
}
