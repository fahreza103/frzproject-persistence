package com.frzproject.springpersistence.entity;


import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.frzproject.springpersistence.util.DateUtil;

import java.util.Date;

@MappedSuperclass
public abstract class GenericWithTimePeriod extends Generic {

	private static final long serialVersionUID = 6471053153515266107L;
	protected TimePeriod validFor;

	public GenericWithTimePeriod() {
		super();
	}

	public GenericWithTimePeriod(long id) {
		super(id);
	}

	public TimePeriod getValidFor() {
		return validFor;
	}

	public void setValidFor(TimePeriod validFor) {
		this.validFor = validFor;
	}

	@Transient
	public boolean isValidPeriod() {
		return isValidPeriod(DateUtil.now());
	}

	public boolean isValidPeriod(Date compareWithDate) {
		return this.getValidFor() == null 
				|| DateUtil.withinValidPeriod(this.getValidFor().getStartDateTime(),
				this.getValidFor().getEndDateTime(), compareWithDate);
	}

}
