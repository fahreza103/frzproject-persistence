package com.frzproject.springpersistence.entity;

public interface BaseEntity {
    long getId();
}
