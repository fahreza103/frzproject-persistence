package com.frzproject.springpersistence.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.frzproject.springpersistence.util.DateUtil;



@Embeddable
public class TimePeriod implements Serializable {

	private static final long serialVersionUID = 288930755087312801L;

	@Column(name = "valid_from_start")
	private Date startDateTime;

	@Column(name = "valid_till_end")
	private Date endDateTime;

	public TimePeriod() {
	}

	public TimePeriod(Date startDate) {
		this.startDateTime = startDate;
	}

	public TimePeriod(Date startDate, Date endDate) {
		this.startDateTime = startDate;
		this.endDateTime = endDate;
	}

	public TimePeriod(LocalDate startDate) {
		this(startDate, null);
	}

	public TimePeriod(LocalDate startDate, LocalDate endDate) {
		this.startDateTime = startDate != null
				? Date.from(startDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())
				: null;
		this.endDateTime = endDate != null
				? Date.from(endDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())
				: null;
	}

	public TimePeriod(LocalDateTime startDateTime, LocalDateTime endDateTime) {
		this.startDateTime = startDateTime != null ? Date.from(startDateTime.atZone(ZoneId.systemDefault()).toInstant())
				: null;
		this.endDateTime = endDateTime != null ? Date.from(endDateTime.atZone(ZoneId.systemDefault()).toInstant())
				: null;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	@Override
	public String toString() {
		return "TimePeriod{" + "\nstartDateTime=" + startDateTime + "\n, endDateTime=" + endDateTime + "\n}";
	}

	/**
	 * Period must have started before the period can end. If the period has not
	 * started, it will be false;
	 * 
	 * @return
	 */
	public boolean isPeriodEnded() {
		return this.startDateTime != null && this.getStartDateTime().before(DateUtil.now())
				&& this.getEndDateTime() != null && this.getEndDateTime().before(DateUtil.now());
	}

	public boolean isPeriodStarted() {
		return this.startDateTime == null || this.getStartDateTime().before(DateUtil.now());
	}

	public boolean isWithinPeriod() {
		return this.startDateTime != null && this.getStartDateTime().before(DateUtil.now())
				&& (this.getEndDateTime() == null || this.getEndDateTime().after(DateUtil.now()));
	}
}
