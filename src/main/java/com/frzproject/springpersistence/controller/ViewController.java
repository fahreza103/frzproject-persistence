package com.frzproject.springpersistence.controller;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.frzproject.springpersistence.model.criteria.CriteriaManagedResource;
import com.frzproject.springpersistence.repository.ViewRepository;
import com.frzproject.springpersistence.specification.SpecificationBuilder;

import io.swagger.annotations.ApiOperation;

/**
 * Common controller for view, only applied to search operation 
 * @author Fahreza Tamara
 *
 * @param <T>
 */
public class ViewController <T,ID extends Serializable> {
	protected ViewRepository<T, ID> viewRepository;
	protected SpecificationBuilder<T> specificationBuilder = new SpecificationBuilder<T>();
	
	public ViewController(ViewRepository<T,ID> viewRepository) {
		this.viewRepository = viewRepository;
	}
	
	@ApiOperation("Common Search API")
	@RequestMapping(value = "search", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<? extends T>> doSearch ( CriteriaManagedResource  criteriaManagedResource) {
		specificationBuilder.setCriteriaManagedResource(criteriaManagedResource);

		preSearch(criteriaManagedResource);
		Page<? extends T> dataResults = search(criteriaManagedResource);
		postSearch(dataResults,criteriaManagedResource);
		return new ResponseEntity<Page<? extends T>>(dataResults, HttpStatus.OK);
	}
	
	protected void postSearch(Page<? extends T> dataResults, CriteriaManagedResource criteriaManagedResource) {}
	
	protected Page<? extends T>  search(CriteriaManagedResource  criteriaManagedResource) {
		Specification<T> specification = specificationBuilder.buildSpecication(criteriaManagedResource.getOrderBy());
		PageRequest pr = PageRequest.of(criteriaManagedResource.getPagingRequest().getCurrentPage(), criteriaManagedResource.getPagingRequest().getSize());
		return viewRepository.findAll(specification, pr);
	}
	protected void preSearch(CriteriaManagedResource  criteriaManagedResource) {}
}
