package com.frzproject.springpersistence.controller;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.frzproject.springpersistence.entity.Generic;
import com.frzproject.springpersistence.model.criteria.CriteriaManagedResource;
import com.frzproject.springpersistence.repository.BasicRepository;
import com.frzproject.springpersistence.specification.SpecificationBuilder;

import io.swagger.annotations.ApiOperation;

/**
 * Basic CRUD Controller to create basic URL Mapping like search,save,gebtById,Update and delete
 * @author Fahreza Tamara
 *
 * @param <T> Entity that extends Generic must be supplied here
 */
public class CrudController <T extends Generic> {
	
	protected BasicRepository<T> basicRepository;
	protected SpecificationBuilder<T> specificationBuilder = new SpecificationBuilder<T>();
//	private final ExampleMatcher exampleMatcher = ExampleMatcher.matching()
//			.withIgnoreCase().withStringMatcher(StringMatcher.CONTAINING);
	
	public CrudController(BasicRepository<T> basicRepository) {
		this.basicRepository = basicRepository;
	}
	
	

	@ApiOperation("Common Search API")
	@RequestMapping(value = "search", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<? extends T>> doSearch (@RequestBody CriteriaManagedResource  criteriaManagedResource) {
		specificationBuilder.setCriteriaManagedResource(criteriaManagedResource);

		preSearch(criteriaManagedResource);
		Page<? extends T> dataResults = search(criteriaManagedResource);
		postSearch(dataResults,criteriaManagedResource);
		return new ResponseEntity<Page<? extends T>>(dataResults, HttpStatus.OK);
	}
	
	@ApiOperation("Common getById API")
	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<T> doGet(@PathVariable("id")long id, HttpServletRequest servletRequest) throws Exception {
		beforeGet(id);
		Optional<T> item = basicRepository.findById(id);
		if (item.get() == null) {
			return new ResponseEntity<T>((T) null, HttpStatus.NOT_FOUND);
		}
		afterGet(item.get());
		return new ResponseEntity<T>(item.get(), HttpStatus.OK);
	}
	
	@ApiOperation("Common save API")
	@RequestMapping(value = "save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<T> doSave(@RequestBody @Valid T item, HttpServletRequest servletRequest) throws Exception {
		beforeSave(item);
		T savedItem = doSave(item);
		afterSave(savedItem);
		return new ResponseEntity<T>(savedItem, HttpStatus.CREATED);
	}
	
	@ApiOperation("Common Update API")
	@RequestMapping(value = "save/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<T> doSave(@PathVariable("id")long id,@RequestBody @Valid T item, HttpServletRequest servletRequest) throws Exception {
		item.setId(id);
		// fetch and update parameter
		Optional<T> original = basicRepository.findById(id);
		if (original.get() == null) {
			return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
		}
		item.setId(id);
		beforePut(item);
		T savedItem = doSave(item);
		afterPut(savedItem);
		return new ResponseEntity<T>(savedItem, HttpStatus.ACCEPTED);
	}
	
	@ApiOperation("Common Delete API")
	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<T> doDelete(@PathVariable("id")long  id, HttpServletRequest servletRequest) throws Exception {
		Optional<T> original = basicRepository.findById(id);
		if (original == null) {
			return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
		}
		beforeDelete(id);
		doDelete(original.get());
		afterDelete(id);
		return new ResponseEntity<T>(HttpStatus.OK);
	}
	
	
	
	protected void afterDelete(long id) {}
	protected void doDelete(T original) {
		basicRepository.delete(original);
	}
	protected void beforeDelete(long id) {}
	protected void afterPut(T savedItem) {
	}
	protected void beforePut(T item) throws Exception {
	}
	protected void afterPatch(T savedItem) {
	}
	protected T doSave(T item) {
		T savedItem = basicRepository.save(item);
		return savedItem;
	}
	protected T performPatch(T original, T request) {
		return original;
	}
	protected void beforePatch(T item) {
	}
	protected void afterGet(T item) {
	}
	protected void beforeGet(long id) {
	}
	protected void postSearch(Page<? extends T> dataResults, CriteriaManagedResource criteriaManagedResource) {}
	
	protected Page<? extends T>  search(CriteriaManagedResource  criteriaManagedResource) {
		Specification<T> specification = specificationBuilder.buildSpecication(criteriaManagedResource.getOrderBy());
		PageRequest pr = PageRequest.of(criteriaManagedResource.getPagingRequest().getCurrentPage(), criteriaManagedResource.getPagingRequest().getSize());
		return basicRepository.findAll(specification, pr);
	}
	protected void preSearch(CriteriaManagedResource  criteriaManagedResource) {}
	
	protected void beforeSave(T savedItem) throws Exception {
	}
	protected void afterSave(T item) throws Exception {
	}
}