# dbase-persistence

**Persistance library for spring boot, this lib contains :**

  - Base CRUD Controller , basic request such as list, findByOne, insert, update, delete
  - Search operation for filter in list, join entity

**To create a new controller in your project, please extend the CrudController class, for example :**

```java
@RestController
@RequestMapping(value = "product/brand")
public class BrandController extends CrudController<Brand> {
	
	@Autowired
	public BrandController(BrandRepository<Brand> brandRepository) {
		super(brandRepository);
	}
}
```

**On entity side, please extend Generic class, for example :**
```java
@Entity
@Table(name="BRAND",schema = "product")
public class Brand extends Generic {
	.....
}
```

**Basic CRUD Request : **

  - **List**
  
    - Request type : POST
	
	- Example URL  : {{base_url}}/product/brand/search?order=desc&page=0&search=&size=15&sort=id
	
	- Request Body : Leave it blank if no filter has been applied

```json
    {
        "group" : "AND",
        "criterias":
        [
            {
                "field":"name",
                "operator":"like",
                "value":"ROLAND"
				
            }
        ],
        "orderBy" : [
        	{
        		"fieldName" : "name",
        		"sortType" : "ASC"
        	}	
        ],
        "pagingRequest" : {
        	"page" : 0,
        	"size" : 10
        }
    }
```

```json
	{
		"group" : "AND",
		"criterias":
		[
			{
				"field":"product.brand.id",
				"operator":"equal",
				"value":"5"

			}
		]
	}
```

```json
	{
		"group" : "AND",
		"criterias":
		[
			{
				"field":"name",
				"operator":"equal",
				"value":"PRD-TLK"

			},
			{
				"group" : "OR",
				"criterias" : 
				[
					{
						"field":"customer.type",
						"operator":"equal",
						"value":"1"
					},
					{
						"field":"customer.desc",
						"operator":"like",
						"value":"pro"						
					}
				]
			}
		]
	}
```
  - **FindById**
  
    - Request type : GET
	
	- Example URL  : {{base_url}}/product/brand/{id}
	
	- Request Body : Blank
	
	
  - **Insert**	
  
    - Request type : POST
	
	- Example URL  : {{base_url}}/product/brand
	
	- Request Body : Include field defined in entity class
	
```json
	{
		"name" : "Data Packet"
		"description" : "Data Packet For Internet"
		"Type" : "Digital"
	}
```

  - **Update**	
  
    - Request type : PUT
	
	- Example URL  : {{base_url}}/product/brand/{id}
	
	- Request Body : Include field defined in entity class (same as insert)
	
	
  - **Delete**	
  
    - Request type : DEL
	
	- Example URL  : {{base_url}}/product/brand/{id}
	
	- Request Body : Blank
